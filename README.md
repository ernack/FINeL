# FINeL programming language

## License
FINeL is distributed under the MIT license.

## How to build
### Build sources
```
$ mkdir Build
$ cd Build
$ cmake ../
$ make
```

### Cross-Compile from GNU for Windows
```
$ mkdir Build
$ cd Build
$ x86_64-w64-mingw32.static-cmake ../
$ make
```

*note*: x86_64-w64-mingw32.static-cmake can be build using mxe (see https://mxe.cc)

### Install
```
$ sudo make install
```

### Run tests
```
$ Test/test
```

## Commands
- **fnlb** the bytecode builder.

## Project structure
- **Lib/**: Main FINeL library : contains Lexer, Parser and Env.
- **Fnlb/**: Generate .fnlo from the AST.
- **Fnli/**: Interpreter for .fnlo files.
- **Natives/**: Native functions of FINeL.
- **Doc/**: FINeL main documentation.
- **Test/** Test framework configuration.
- **Thirds/** Thirds libraries used by FINeL.

