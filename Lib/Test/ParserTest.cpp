#include <sstream>
#include <vector>
#include <catch.hpp>
#include "../Src/Lexer.hpp"
#include "../Src/Parser.hpp"
#include "../Src/Node.hpp"
#include "../Src/Value.hpp"

TEST_CASE("ParserEmpty", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	
	REQUIRE(nullptr == root);
}

TEST_CASE("ParserInt", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("43");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	
	REQUIRE("NODE_INT(43)" == root->string());
}

TEST_CASE("ParserByte", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("73b");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	
	REQUIRE("NODE_BYTE(73)" == root->string());
}

TEST_CASE("ParserUint", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("27u");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	
	REQUIRE("NODE_UINT(27)" == root->string());
}

TEST_CASE("ParserFloat", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("33.5");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);

	REQUIRE("NODE_FLOAT(33.5)" == root->string());
}

TEST_CASE("ParserBoolTrue", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("true");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);

	REQUIRE("NODE_BOOL(true)" == root->string());
}

TEST_CASE("ParserBoolFalse", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("false");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);

	REQUIRE("NODE_BOOL(false)" == root->string());
}

TEST_CASE("ParserString", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("\"coucou\"");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);

	REQUIRE("NODE_STRING(coucou)" == root->string());
}

TEST_CASE("ParserSymbol", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("'queen");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);

	REQUIRE("NODE_SYMBOL(queen)" == root->string());
}

TEST_CASE("ParserIdent", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("hola");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE("NODE_IDENT(hola)" == root->string());
}

TEST_CASE("ParserDeclareVariable", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("[pizza 'cheese]");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE("NODE_DECLARE_VAR(NODE_IDENT(pizza),NODE_SYMBOL(cheese))" == root->string());
}

TEST_CASE("ParserFunctionCall", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("(add 2 4 6 8)");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE("NODE_FUNCALL(NODE_IDENT(add),NODE_INT(2),NODE_INT(4),NODE_INT(6),NODE_INT(8))" ==
					root->string());
}

TEST_CASE("ParserFunctionCallTwoIdents", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("(sub x y)");

	fnl::Parser parser;

	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE("NODE_FUNCALL(NODE_IDENT(sub),NODE_IDENT(x),NODE_IDENT(y))" ==
					root->string());
}

TEST_CASE("ParserFunctionLiteral", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("[[x y] (mul x y)]");

	fnl::Parser parser;
	
	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE(nullptr != root);
	std::stringstream ss;

	ss << "NODE_FUNCTION(NODE_PARAMS(NODE_IDENT(x),NODE_IDENT(y)),NODE_BODY(NODE_FUNCALL(NODE_IDENT(mul),NODE_IDENT(x),NODE_IDENT(y))))";
	
	REQUIRE(ss.str() == root->string());
}

TEST_CASE("ParserDeclareFunction", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("[add [[x y] (mul x y)]]");

	fnl::Parser parser;
	
	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE(nullptr != root);
	std::stringstream ss;

	ss << "NODE_DECLARE_VAR(NODE_IDENT(add),"
		 << "NODE_FUNCTION(NODE_PARAMS(NODE_IDENT(x),NODE_IDENT(y)),"
		 << "NODE_BODY(NODE_FUNCALL(NODE_IDENT(mul),NODE_IDENT(x),NODE_IDENT(y)))))";
	
	REQUIRE(ss.str() == root->string());
}

TEST_CASE("ParserMultiIdent", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("aze zer ert rty");

	fnl::Parser parser;
	
	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE(nullptr != root);
	std::stringstream ss;

	ss << "NODE_FINEL(NODE_IDENT(aze),NODE_IDENT(zer),NODE_IDENT(ert),NODE_IDENT(rty))";
	
	REQUIRE(ss.str() == root->string());
}

TEST_CASE("ParserMultiExpr", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("[x 16b] x");

	fnl::Parser parser;
	
	std::shared_ptr<fnl::Node> root = parser(tokens);
	REQUIRE(nullptr != root);
	std::stringstream ss;

	ss << "NODE_FINEL(NODE_DECLARE_VAR(NODE_IDENT(x),NODE_BYTE(16)),NODE_IDENT(x))";
	
	REQUIRE(ss.str() == root->string());
}
