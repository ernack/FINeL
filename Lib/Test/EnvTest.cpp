#include <vector>
#include <catch.hpp>
#include "../Src/AST.hpp"
#include "../Src/Lexer.hpp"
#include "../Src/Parser.hpp"
#include "../Src/Node.hpp"
#include "../Src/Env.hpp"

TEST_CASE("EnvDefineAndGetUnknownVarError", "[Env]")
{
	fnl::Lexer lexer;
	fnl::Parser parser;
	std::shared_ptr<fnl::Node> root = parser(lexer("34"));

	fnl::Env env;
	env.declare("azee", root);
	
	REQUIRE_THROWS(env.get("bouh"));
}

TEST_CASE("EnvDefineAndUpdateUnknownVarError", "[Env]")
{
	fnl::Lexer lexer;
	fnl::Parser parser;
	std::shared_ptr<fnl::Node> root = parser(lexer("34"));

	fnl::Env env;
	env.declare("azee", root);
	
	REQUIRE_THROWS(env.update("bouh", root));
}

TEST_CASE("EnvDefineRedeclareError", "[Env]")
{
	fnl::Lexer lexer;
	fnl::Parser parser;
	std::shared_ptr<fnl::Node> root = parser(lexer("34"));

	fnl::Env env;
	env.declare("azee", root);
	
	REQUIRE_THROWS(env.declare("azee", root));
}


TEST_CASE("EnvDefineAndGet", "[Env]")
{
	fnl::Lexer lexer;
	fnl::Parser parser;
	std::shared_ptr<fnl::Node> root = parser(lexer("34"));

	fnl::Env env;
	env.declare("azee", root);
	
	REQUIRE("NODE_INT(34)" == env.get("azee")->string());
}

TEST_CASE("EnvUpdate", "[Env]")
{
	fnl::Lexer lexer;
	fnl::Parser parser;
	std::shared_ptr<fnl::Node> root = parser(lexer("34"));
	std::shared_ptr<fnl::Node> root2 = parser(lexer("'chien"));
	fnl::Env env;
	
	env.declare("azee", root);
	env.update("azee", root2);
	
	REQUIRE("NODE_SYMBOL(chien)" == env.get("azee")->string());
}

TEST_CASE("EnvBlock", "[Env]")
{
	fnl::AST ast;
	fnl::Env env;
	
	env.declare("outside", ast("57"));

	env.open_block();
	env.declare("inside", ast("2.4"));
	REQUIRE("NODE_INT(57)" == env.get("outside")->string());
	REQUIRE("NODE_FLOAT(2.4)" == env.get("inside")->string());
	env.declare("outside", ast("'salut"));
	REQUIRE("NODE_SYMBOL(salut)" == env.get("outside")->string());
	env.close_block();
	
	REQUIRE("NODE_INT(57)" == env.get("outside")->string());
	REQUIRE_THROWS(env.get("inside"));
}
