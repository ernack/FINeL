#include <vector>
#include <catch.hpp>
#include "../Src/Lexer.hpp"
#include "../Src/Parser.hpp"
#include "../Src/Node.hpp"
#include "../Src/Value.hpp"

TEST_CASE("LexerBugKeywordEnd", "[LexerBug]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("falseee");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_IDENT == tokens.back().type());
	REQUIRE(fnl::VALUE_STRING == tokens.back().value().type());
	REQUIRE("falseee" == tokens.back().value().as_string());
}

TEST_CASE("LexerBugIdentOrKeywordEnd", "[LexerBug]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("false");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_BOOL == tokens.back().type());
	REQUIRE(fnl::VALUE_BOOL == tokens.back().value().type());
	REQUIRE(false == tokens.back().value().as_bool());
}

TEST_CASE("LexerBugString", "[LexerBug]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("\"zsh is better than bash\"");

	REQUIRE(1 == tokens.size());
	REQUIRE("zsh is better than bash" == tokens.back().value().as_string());
}

TEST_CASE("ParserBugString", "[ParserBug]")
{
	fnl::Lexer lexer;
	fnl::Parser parser;
  auto root = parser(lexer("\"zsh is better than bash\""));

	REQUIRE("NODE_STRING(zsh is better than bash)" == root->string());
}

TEST_CASE("LexerBugCloseSquare", "[LexerBug]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("[eat 'pizza]");

	REQUIRE(4 == tokens.size());
	REQUIRE(fnl::TOKEN_OPEN_SQUARE == tokens.at(0).type());
	REQUIRE(fnl::TOKEN_IDENT == tokens.at(1).type());
	REQUIRE(fnl::TOKEN_SYMBOL == tokens.at(2).type());
	REQUIRE(fnl::TOKEN_CLOSE_SQUARE == tokens.at(3).type());

}
