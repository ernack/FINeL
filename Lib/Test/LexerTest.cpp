#include <vector>
#include <catch.hpp>
#include "../Src/Lexer.hpp"
#include "../Src/Value.hpp"

TEST_CASE("LexerEmpty", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("");

	REQUIRE(0 == tokens.size());
}

TEST_CASE("LexerInt", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("32");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_INT == tokens.back().type());
	REQUIRE(32 == tokens.back().value().as_int());
}

TEST_CASE("LexerNegativeInt", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("-4");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_INT == tokens.back().type());
	REQUIRE(-4 == tokens.back().value().as_int());
}

TEST_CASE("LexerByte", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("16b");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_BYTE == tokens.back().type());
	REQUIRE(16 == tokens.back().value().as_byte());
}

TEST_CASE("LexerUint", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("2567u");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_UINT == tokens.back().type());
	REQUIRE(2567 == tokens.back().value().as_uint());
}

TEST_CASE("LexerFloat", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("27.2");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_FLOAT == tokens.back().type());
	REQUIRE(Approx(27.2) == tokens.back().value().as_float());
}

TEST_CASE("LexerBoolTrue", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("true");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_BOOL == tokens.back().type());
	REQUIRE(true == tokens.back().value().as_bool());
}

TEST_CASE("LexerBoolFalse", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("false");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_BOOL == tokens.back().type());
	REQUIRE(false == tokens.back().value().as_bool());
}

TEST_CASE("LexerStringEmpty", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("\"\"");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_STRING == tokens.back().type());
	REQUIRE("" == tokens.back().value().as_string());
}

TEST_CASE("LexerString", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("\"this is sparta\"");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_STRING == tokens.back().type());
	REQUIRE("this is sparta" == tokens.back().value().as_string());
}

TEST_CASE("LexerSymbol", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("'hello");

	REQUIRE(1 == tokens.size());
	REQUIRE(fnl::TOKEN_SYMBOL == tokens.back().type());
	REQUIRE("hello" == tokens.back().value().as_string());	
	REQUIRE(std::hash<std::string>{}("hello") == tokens.back().value().as_symbol());
}

TEST_CASE("LexerDeclareVariable", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("[hello 34]");

	REQUIRE(4 == tokens.size());
	REQUIRE(fnl::TOKEN_OPEN_SQUARE == tokens.at(0).type());
	REQUIRE(fnl::TOKEN_IDENT == tokens.at(1).type());
	REQUIRE("hello" == tokens.at(1).value().as_string());
	REQUIRE(fnl::TOKEN_INT == tokens.at(2).type());
	REQUIRE(34 == tokens.at(2).value().as_int());
	REQUIRE(fnl::TOKEN_CLOSE_SQUARE == tokens.at(3).type()); 
}

TEST_CASE("LexerFunctionCall", "[AST]")
{
	fnl::Lexer lexer;
	std::vector<fnl::Token> tokens = lexer("(sub 5 6 7)");

	REQUIRE(6 == tokens.size());
	REQUIRE(fnl::TOKEN_OPEN_PAR == tokens.at(0).type());
	REQUIRE(fnl::TOKEN_CLOSE_PAR == tokens.at(5).type()); 
}
