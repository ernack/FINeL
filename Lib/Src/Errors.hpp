#ifndef VALUEERRORS_HPP
#define VALUEERRORS_HPP

#include <stdexcept>
#include "Value.hpp"

namespace fnl
{
	class value_mismatch_error : public std::runtime_error
	{
	public:
		value_mismatch_error(value_type_t want, value_type_t have)
			: std::runtime_error("value mismatch error, want " + value_type_string(want) + " have " + value_type_string(have))
		{
		}
	};

	class parser_unknown_token_error : public std::runtime_error
	{
	public:
	  parser_unknown_token_error()
			: std::runtime_error("parser unknown token error")
		{
		}
	};
	
	class parser_unknown_value_error : public std::runtime_error
	{
	public:
	  parser_unknown_value_error()
			: std::runtime_error("parser unknown value error")
		{
		}
	};

	class env_unknown_var_error : public std::runtime_error
	{
	public:
	  env_unknown_var_error(std::string const& varname)
			: std::runtime_error("env unknown var error: " + varname)
		{
		}
	};

	class env_redeclare_var_error : public std::runtime_error
	{
	public:
	  env_redeclare_var_error(std::string const& varname)
			: std::runtime_error("env redeclare var error: " + varname)
		{
		}
	};

}
#endif // VALUEERRORS

