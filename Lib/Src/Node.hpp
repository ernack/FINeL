#ifndef NODE_HPP
#define NODE_HPP

#include <vector>
#include <memory>
#include <string>
#include "Value.hpp"

namespace fnl
{
	typedef enum {
								NODE_INT,
								NODE_BYTE,
								NODE_UINT,
								NODE_FLOAT,
								NODE_BOOL,
								NODE_STRING,
								NODE_SYMBOL,
								NODE_DECLARE_VAR,
								NODE_IDENT,
								NODE_FUNCALL,
								NODE_FUNCTION,
								NODE_PARAMS,
								NODE_BODY,
								NODE_FINEL
	} node_type_t;

	std::string node_type_string(node_type_t type);
	
	class Node
	{
  public:
		explicit Node(node_type_t type, Value value);

		std::string string() const;

		void add_child(std::shared_ptr<Node> child);
		std::shared_ptr<Node> const child(size_t index) const;
	  size_t children_count() const;
		
		Value value() const;		
		node_type_t type() const;
		
		virtual ~Node();
		Node& operator=(Node const& node) = delete;
		Node(Node const& node) = delete;
  private:		
		node_type_t m_type;
		Value m_value;

		std::vector<std::shared_ptr<Node>> m_children;
	};
}
#endif // NODE

