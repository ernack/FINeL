#ifndef AST_HPP
#define AST_HPP
#include <memory>
#include <string>
#include "Node.hpp"

namespace fnl
{
	class AST
	{
  public:
		explicit AST();

		std::shared_ptr<Node> operator()(std::string const& source);
		
		virtual ~AST();
		AST& operator=(AST const& ast) = delete;
		AST(AST const& ast) = delete;
  private:
	};
}
#endif // AST

