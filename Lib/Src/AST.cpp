#include "AST.hpp"
#include "Lexer.hpp"
#include "Parser.hpp"

namespace fnl
{
	/*explicit*/ AST::AST()
	{
	}	

  std::shared_ptr<Node> AST::operator()(std::string const& source)
	{
		Lexer lexer;
		Parser parser;

		return parser(lexer(source));
	}
	
	/*virtual*/ AST::~AST()
	{
	}
}
