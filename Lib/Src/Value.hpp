#ifndef VALUE_HPP
#define VALUE_HPP

#include <string>

namespace fnl
{
	typedef enum {
								VALUE_INT,
								VALUE_BYTE,
								VALUE_UINT,
								VALUE_FLOAT,
								VALUE_BOOL,
								VALUE_STRING,
								VALUE_SYMBOL
	} value_type_t;

	std::string value_type_string(value_type_t type);
	
	class Value
	{
  public:
		explicit Value(value_type_t type = VALUE_INT);
		Value& operator=(Value const& value);

		value_type_t type() const;
		
		Value(Value const& value);

		std::string string() const;
		std::string inspect() const;
		
		int as_int() const;
		void set_int(int value);
		
		char as_byte() const;
		void set_byte(char value);
		
		unsigned int as_uint() const;
		void set_uint(unsigned int value);
		
		double as_float() const;
		void set_float(double value);

		bool as_bool() const;
		void set_bool(bool value);

		std::string as_string() const;
		void set_string(std::string value);

	  size_t as_symbol() const;
		void set_symbol(std::string from_value);

		virtual ~Value();	 

  private:
		value_type_t m_type;
		union {
			int i; /* int */
			char b; /* byte */
			unsigned int ui; /* uint */
			double f; /* float */
			bool bo; /* bool */
		  size_t s; /* symbol */
		} m_native_value;
		std::string m_string_value;		
	};
}
#endif // VALUE

