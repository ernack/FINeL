#include <iostream>
#include "Lexer.hpp"
#include "Value.hpp"

namespace fnl
{
	/*explicit*/ Lexer::Lexer()
	{
	}
	
	std::vector<Token> Lexer::operator()(std::string const& source)
	{
		std::vector<Token> tokens;
		
		m_source = source;
		m_cursor = 0;

		while (m_cursor < m_source.size())
			{
				// Blank stuff
				if (m_source.at(m_cursor) == '\n' ||
						m_source.at(m_cursor) == '\t')
					{
						m_cursor++;
						continue;
					}

				if (match("("))
					{
						tokens.push_back(Token(TOKEN_OPEN_PAR, nullptr));
						continue;
					}

				if (match(")"))
					{
						tokens.push_back(Token(TOKEN_CLOSE_PAR, nullptr));
						continue;
					}
												
				if (match("["))
					{
						tokens.push_back(Token(TOKEN_OPEN_SQUARE, nullptr));
						continue;
					}
				
				if (match("]"))
					{
						tokens.push_back(Token(TOKEN_CLOSE_SQUARE, nullptr));
						continue;
					}

				if (match_keyword("true"))
					{
						auto value = std::make_unique<Value>(VALUE_BOOL);
						value->set_bool(true);
						tokens.push_back(Token(TOKEN_BOOL, std::move(value)));
						continue;
					}

				if (match_keyword("false"))
					{
						auto value = std::make_unique<Value>(VALUE_BOOL);
						value->set_bool(false);
						tokens.push_back(Token(TOKEN_BOOL, std::move(value)));
						continue;
					}

				auto got_symbol = match_symbol();
				
				if (got_symbol.second)
					{
						auto value = std::make_unique<Value>(VALUE_SYMBOL);
						value->set_symbol(got_symbol.first);						
						tokens.push_back(Token(TOKEN_SYMBOL, std::move(value)));
						continue;
					}

				auto got_string = match_string();
				
				if (got_string.second)
					{
						auto value = std::make_unique<Value>(VALUE_STRING);
						value->set_string(got_string.first);
						tokens.push_back(Token(TOKEN_STRING, std::move(value)));
						continue;
					}

				auto got_ident = match_ident();
				
				if (got_ident.second)
					{
						auto value = std::make_unique<Value>(VALUE_STRING);
						value->set_string(got_ident.first);
						tokens.push_back(Token(TOKEN_IDENT, std::move(value)));
						
						continue;
					}

				auto got_float = match_float();
				
				if (got_float.second)						
					{
						auto value = std::make_unique<Value>(VALUE_FLOAT);
						value->set_float(got_float.first);
						tokens.push_back(Token(TOKEN_FLOAT, std::move(value)));
						continue;
					}

				auto got_byte = match_decorated_int("b");
				
				if (got_byte.second)						
					{
						auto value = std::make_unique<Value>(VALUE_BYTE);
						value->set_byte(got_byte.first);
						tokens.push_back(Token(TOKEN_BYTE, std::move(value)));
						continue;
					}

				auto got_uint = match_decorated_int("u");
				
				if (got_uint.second)						
					{
						auto value = std::make_unique<Value>(VALUE_UINT);
						value->set_uint(got_uint.first);
						tokens.push_back(Token(TOKEN_UINT, std::move(value)));
						continue;
					}

				auto got_int = match_int();
				
				if (got_int.second)						
					{
						auto value = std::make_unique<Value>(VALUE_INT);
						value->set_int(got_int.first);
						tokens.push_back(Token(TOKEN_INT, std::move(value)));
						continue;
					}

				m_cursor++;
			}
		
		return tokens;
	}

	bool Lexer::match_try(std::function<bool()> body)
	{
		size_t saved = m_cursor;
		bool success = body();
		if (!success) { m_cursor = saved; }
		return success;
	}
	
	bool Lexer::match(std::string const& text)
	{
		if (m_cursor >= m_source.size()) { return false; }
		
		size_t size = std::min(text.size(), m_source.size());
		
		for (size_t i=0; i<size; i++)
			{
				if (m_source.at(m_cursor + i) != text.at(i))
					{
						return false;
					}
			}

		m_cursor += size;

		return true;				
	}

	bool Lexer::match_keyword(std::string const& text)
	{
		return match_try([this, &text](){
											 return match(text) && match_sep();
										 });
	}
	
	bool Lexer::match_sep()
	{
		return m_cursor >= m_source.size() ||
			m_source.at(m_cursor) == ' ' ||
			m_source.at(m_cursor) == '\t' ||
			m_source.at(m_cursor) == '\n'
			;
	}

	bool Lexer::match_string_sep()
	{
		return match_sep() ||
			m_source.at(m_cursor) == ']' ||
			m_source.at(m_cursor) == '[' ||
			m_source.at(m_cursor) == ')' ||
			m_source.at(m_cursor) == '('
			;
	}

	std::pair<std::string, bool> Lexer::match_ident()
	{
		std::string value;

		bool success =
			match_try([this, &value](){

									if (m_source.at(m_cursor) == '-' &&
											m_source.at(m_cursor + 1) >= '0' &&
											m_source.at(m_cursor + 1) <= '9')
										{
											return false;
										}
									
									if (m_source.at(m_cursor) >= '0' &&
											m_source.at(m_cursor) <= '9')
										{
											return false;
										}
									
									while (m_cursor < m_source.size() && !match_string_sep())
										{
											char next = m_source.at(m_cursor);
											
											if (next != '(' &&
													next != ')' &&
													next != '[' &&
													next != ']')
												{
													value += next;
													m_cursor++;
												} else { m_cursor--; break; }																				
										}
									
									return value != "" && match_string_sep();
								});

		if (success) { return {value, true}; }
		return {"", false};
	}
	
	std::pair<std::string, bool> Lexer::match_symbol()
	{
		std::string value;
		
		bool success =
			match_try([this, &value](){
									if (!match("'")) { return false; }

									while (m_cursor < m_source.size() && !match_string_sep())
										{
											value += m_source.at(m_cursor);
											m_cursor++;
										}
									
									return match_string_sep();
								});
		
		if (success) { return {value, true}; }
		return {"", false};
	}
	
	std::pair<std::string, bool> Lexer::match_string()
	{
		std::string value;
		
		bool success =
			match_try([this, &value](){
									if (!match("\"")) { return false; }

									while (m_cursor < m_source.size())
										{
											value += m_source.at(m_cursor);
											
											if (match("\"")) { return true; }
											m_cursor++;
										}

									return false;
								});
		
		if (success) { return {value.substr(0, value.size()-1), true}; }
		return {"", false};
		
	}
	
	std::pair<double, bool> Lexer::match_float()
	{
		std::string value;
		
		bool success =
			match_try([this, &value](){
									match("-");
		
									auto i0 = match_int();
									bool dot = match(".");
									auto i1 = match_int();
		
									if (!i0.second || !dot || !i1.second)
										{
											return false;
										}

									value = std::to_string(i0.first) +
										"." +
										std::to_string(i1.first);

									return true;
								});

		if (success) { return {std::stof(value), true}; }
		else { return {0, false}; }
	}
	
	std::pair<int, bool> Lexer::match_decorated_int(std::string const& decorator)
	{
		int value = 0;

		bool success =
			match_try([this, &value, decorator](){
									auto val = match_int();
									if (!match(decorator)) { return false; }
													 
									if (!val.second) { return false; }
									value = val.first;
									
									return true;
								});

		if (success) { return {value, true}; }
		return {0, false};
	}
	
	std::pair<int, bool> Lexer::match_int()
	{				
		std::string str_value;

		bool success =
			match_try([this, &str_value](){
									if (m_cursor >= m_source.size()) { return false; }
				
									if (match("-")) { str_value = "-"; }
									bool one_digit = false;
				
									while (m_cursor < m_source.size())
										{
											auto digit = match_digit();
											if (!digit.second) { break; }
											one_digit = true;
											str_value += std::to_string(digit.first);
										}

									if (!one_digit) { return false; }
									return true;
								});
		
		if (success) { return {std::stoi(str_value), true}; }
		return {0, false};
	}
	
	std::pair<int, bool> Lexer::match_digit()
	{
		if (m_cursor >= m_source.size()) { return {0, false}; }
		
		if (m_source.at(m_cursor) >= '0' &&
				m_source.at(m_cursor) <= '9')
			{
				m_cursor++;
				return { m_source.at(m_cursor - 1) - '0', true };
			}

		return {0, false};
	}
	
	/*virtual*/ Lexer::~Lexer()
	{
	}
}
