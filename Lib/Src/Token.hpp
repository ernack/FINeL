#ifndef TOKEN_HPP
#define TOKEN_HPP

#include <memory>

namespace fnl
{
	typedef enum {
								TOKEN_INT,
								TOKEN_BYTE,
								TOKEN_UINT,
								TOKEN_FLOAT,
								TOKEN_BOOL,
								TOKEN_STRING,
								TOKEN_SYMBOL,
								TOKEN_OPEN_SQUARE,
								TOKEN_CLOSE_SQUARE,
								TOKEN_IDENT,
								TOKEN_OPEN_PAR,
								TOKEN_CLOSE_PAR

	} token_type_t;

	std::string token_type_string(token_type_t type);
	
	class Value;
	
	class Token
	{
  public:
		explicit Token(token_type_t type, std::unique_ptr<Value> value);
		Token(Token const& token);
		
		token_type_t type() const;
		Value const& value() const;
		
		virtual ~Token();
		Token& operator=(Token const& token) = delete;
		
  private:
		token_type_t m_type;
		std::unique_ptr<Value> m_value;
		
	};
}
#endif // TOKEN

