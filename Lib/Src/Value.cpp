#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip>
#include "Value.hpp"
#include "Errors.hpp"

namespace fnl
{
	std::string value_type_string(value_type_t type)
	{
		std::vector<std::string> names = {"VALUE_INT",
																			"VALUE_BYTE",
																			"VALUE_UINT",
																			"VALUE_FLOAT",
																			"VALUE_BOOL",
																			"VALUE_STRING",
																			"VALUE_SYMBOL"};
		return names[type];
	}
	
	/*explicit*/ Value::Value(value_type_t type)
		: m_type { type }
	{
	}

	Value::Value(Value const& value)
	{
		*this = value;
	}

	Value& Value::operator=(Value const& value)
	{
		m_type = value.m_type;
		m_native_value = value.m_native_value;
		m_string_value = value.m_string_value;
		return *this;
	}

	value_type_t Value::type() const
	{
		return m_type;
	}
	
	std::string Value::string() const
	{
		switch (m_type)
			{
			case VALUE_INT: return std::to_string(m_native_value.i);
			case VALUE_BYTE: return std::to_string(m_native_value.b);
			case VALUE_UINT: return std::to_string(m_native_value.ui);
			case VALUE_FLOAT: {
				std::stringstream ss;
				ss << std::setprecision(3) << m_native_value.f;
				return ss.str();
			}
			case VALUE_BOOL: {
				std::stringstream ss;
				ss << std::boolalpha << m_native_value.bo;
				return ss.str();
			}
			case VALUE_STRING: return m_string_value;
			case VALUE_SYMBOL: return m_string_value;
			default: throw parser_unknown_value_error();
			}
	}

	std::string Value::inspect() const
	{
		switch (m_type)
			{
			case VALUE_INT: return std::to_string(m_native_value.i);
			case VALUE_BYTE: return std::to_string(m_native_value.b) + "b";
			case VALUE_UINT: return std::to_string(m_native_value.ui) + "u";
			case VALUE_FLOAT: {
				std::stringstream ss;
				ss << std::setprecision(3) << m_native_value.f;
				return ss.str();
			}
			case VALUE_BOOL: {
				std::stringstream ss;
				ss << std::boolalpha << m_native_value.bo;
				return ss.str();
			}
			case VALUE_STRING: return "\"" + m_string_value + "\"";
			case VALUE_SYMBOL: return "'" + m_string_value;
			default: throw parser_unknown_value_error();
			}
	}
	
	int Value::as_int() const
	{
		if (m_type != VALUE_INT)
			{				
				throw value_mismatch_error(m_type, VALUE_INT);
			}

		return m_native_value.i;
	}

	void Value::set_int(int value)
	{
		m_type = VALUE_INT;
		m_native_value.i = value;
	}

	char Value::as_byte() const
	{
		if (m_type != VALUE_BYTE)
			{				
				throw value_mismatch_error(m_type, VALUE_BYTE);
			}

		return m_native_value.b;
	}

	void Value::set_byte(char value)
	{
		m_type = VALUE_BYTE;
		m_native_value.b = value;
	}

	unsigned int Value::as_uint() const
	{
		if (m_type != VALUE_UINT)
			{				
				throw value_mismatch_error(m_type, VALUE_UINT);
			}

		return m_native_value.ui;
	}

	void Value::set_uint(unsigned int value)
	{
		m_type = VALUE_UINT;
		m_native_value.ui = value;
	}

	double Value::as_float() const
	{
		if (m_type != VALUE_FLOAT)
			{				
				throw value_mismatch_error(m_type, VALUE_FLOAT);
			}

		return m_native_value.f;
	}

	void Value::set_float(double value)
	{
		m_type = VALUE_FLOAT;
		m_native_value.f = value;
	}

	bool Value::as_bool() const
	{
		if (m_type != VALUE_BOOL)
			{				
				throw value_mismatch_error(m_type, VALUE_BOOL);
			}

		return m_native_value.bo;
	}
	
	void Value::set_bool(bool value)
	{
		m_type = VALUE_BOOL;
		m_native_value.bo = value;
	}

	std::string Value::as_string() const
	{
		if (m_type != VALUE_STRING && m_type != VALUE_SYMBOL)
			{				
				throw value_mismatch_error(m_type, VALUE_STRING);
			}

		return m_string_value;
	}
	
	void Value::set_string(std::string value)
	{
		m_type = VALUE_STRING;
		m_string_value = value;
	}

  size_t Value::as_symbol() const
	{
		if (m_type != VALUE_SYMBOL)
			{				
				throw value_mismatch_error(m_type, VALUE_SYMBOL);
			}
		
		return m_native_value.s;
	}
	
	void Value::set_symbol(std::string from_value)
	{
		m_type = VALUE_SYMBOL;
		m_native_value.s = std::hash<std::string>{}(from_value);
		m_string_value = from_value;
	}

	/*virtual*/ Value::~Value()
	{
	}
}
