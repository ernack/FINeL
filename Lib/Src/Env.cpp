#include "Errors.hpp"
#include "Env.hpp"
#include "Node.hpp"

namespace fnl
{
	void EnvScope::declare(std::string const& name, std::shared_ptr<Node> node)
	{
		auto itr = table.find(name);

		if (itr != table.end())
			{
				throw env_redeclare_var_error(name);
			}

	  table[name] = node;
	}

	void EnvScope::update(std::string const& name, std::shared_ptr<Node> node)
	{
		auto itr = table.find(name);

		if (itr == table.end())
			{
				throw env_unknown_var_error(name);
			}
		
	  table[name] = node;
	}
	
	std::shared_ptr<Node> EnvScope::get(std::string const& name)
	{
		auto itr = table.find(name);

		if (itr == table.end())
			{
				if (prev)
					{
						return prev->get(name);
					}
				
				throw env_unknown_var_error(name);
			}

		return table[name];
	}
	
	/*explicit*/ Env::Env()
		: m_scope { std::make_unique<EnvScope>() }
	{
	}
	
	void Env::declare(std::string const& name, std::shared_ptr<Node> node)
	{
		m_scope->declare(name, node);
	}

	void Env::update(std::string const& name, std::shared_ptr<Node> node)
	{
		m_scope->update(name, node);
	}
	
	std::shared_ptr<Node> Env::get(std::string const& name)
	{
		return m_scope->get(name);
	}

	void Env::open_block()
	{		
		auto new_scope = std::make_unique<EnvScope>();
		new_scope->prev = std::move(m_scope);
		m_scope = std::move(new_scope);
	}
	
	void Env::close_block()
	{
		auto prev = std::move(m_scope->prev);
		m_scope = std::move(prev);
	}
	
	/*virtual*/ Env::~Env()
	{
	}
}
