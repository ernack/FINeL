#include <cassert>
#include <iostream>
#include <algorithm>
#include <vector>
#include "Node.hpp"

namespace fnl
{
	std::string node_type_string(node_type_t type)
	{
		std::vector<std::string> names = {"NODE_INT",
																			"NODE_BYTE",
																			"NODE_UINT",
																			"NODE_FLOAT",
																			"NODE_BOOL",
																			"NODE_STRING",
																			"NODE_SYMBOL",
																			"NODE_DECLARE_VAR",
																			"NODE_IDENT",
																			"NODE_FUNCALL",
																			"NODE_FUNCTION",
																			"NODE_PARAMS",
																			"NODE_BODY",
																			"NODE_FINEL"};
		return names.at(type);
	}
	
	/*explicit*/ Node::Node(node_type_t type, Value value)
		: m_type { type }
		, m_value { value }
	{
	}
	
	std::string Node::string() const
	{
		std::string name = node_type_string(m_type);
		std::transform(name.begin(), name.end(), name.begin(), toupper);

		name += "(";
		
		if ( !m_children.empty() )
			{
				name += m_children.at(0)->string();
				for (size_t i=1; i<m_children.size(); i++)
					{
						name += "," + m_children.at(i)->string();
					}		
			}
		else
			{				
				name += m_value.string();
			}
		
		name += ")";
		
		return name;
	}

	void Node::add_child(std::shared_ptr<Node> child)
	{
		assert(child);
		m_children.push_back(std::move(child));
	}

	std::shared_ptr<Node> const Node::child(size_t index) const
	{
		assert(index < m_children.size());
		return m_children.at(index);
	}

	size_t Node::children_count() const
	{
		return m_children.size();
	}
	
	Value Node::value() const
	{
		return m_value;
	}
	
	node_type_t Node::type() const
	{
		return m_type;
	}
	
	/*virtual*/ Node::~Node()
	{
	}
}
