#ifndef LEXER_HPP
#define LEXER_HPP

#include <functional>
#include <vector>
#include <string>

#include "Token.hpp"

namespace fnl
{
	class Lexer
	{
  public:
		explicit Lexer();

		std::vector<Token> operator()(std::string const& source);
		
		virtual ~Lexer();
		Lexer& operator=(Lexer const& lexer) = delete;
		Lexer(Lexer const& lexer) = delete;
  private:
		std::string m_source;
		size_t m_cursor;

		bool match_try(std::function<bool()> body);
		bool match(std::string const& text);		
		bool match_keyword(std::string const& text);
		bool match_sep();
		bool match_string_sep();
		std::pair<std::string, bool> match_ident();
		std::pair<std::string, bool> match_symbol();
		std::pair<std::string, bool> match_string();
		std::pair<double, bool> match_float();
		std::pair<int, bool> match_decorated_int(std::string const& decorator);
		std::pair<int, bool> match_int();
		std::pair<int, bool> match_digit();
	};
}
#endif // LEXER

