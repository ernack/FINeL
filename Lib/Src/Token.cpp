#include <vector>
#include <cassert>
#include "Token.hpp"
#include "Value.hpp"

namespace fnl
{
	std::string token_type_string(token_type_t type)
	{
		std::vector<std::string> names = {"TOKEN_INT",
																			"TOKEN_BYTE",
																			"TOKEN_UINT",
																			"TOKEN_FLOAT",
																			"TOKEN_BOOL",
																			"TOKEN_STRING",
																			"TOKEN_SYMBOL",
																			"TOKEN_OPEN_SQUARE",
																			"TOKEN_CLOSE_SQUARE",
																			"TOKEN_IDENT",
																			"TOKEN_OPEN_PAR",
																			"TOKEN_CLOSE_PAR"};
		return names[type];
	}

	/*explicit*/ Token::Token(token_type_t type, std::unique_ptr<Value> value)
		: m_type { type }	
	{
		if (value) { m_value = std::move(value); }
	}

	Token::Token(Token const& token)
	{
		m_type = token.m_type;
		
		if (token.m_value)
			{
				m_value = std::make_unique<Value>(*token.m_value);
			}
	}
	
	token_type_t Token::type() const
	{
		return m_type;
	}
	
	Value const& Token::value() const
	{
		assert(m_value);
		return *m_value;
	}
	
	/*virtual*/ Token::~Token()
	{
	}
}
