#ifndef PARSER_HPP
#define PARSER_HPP

#include <memory>
#include <vector>
#include "Token.hpp"

namespace fnl
{
	class Node;
	
	class Parser
	{
  public:
		explicit Parser();

		std::shared_ptr<Node> operator()(std::vector<Token> const& tokens);
		
		virtual ~Parser();
		Parser& operator=(Parser const& parser) = delete;
		Parser(Parser const& parser) = delete;
  private:
		size_t m_cursor;
	  bool m_is_end_reached = false;
		std::shared_ptr<Node> FINEL(std::vector<Token> const& tokens);
		std::shared_ptr<Node> EXPR(std::vector<Token> const& tokens);
		std::shared_ptr<Node> LITERAL(std::vector<Token> const& tokens);
	};
}
#endif // PARSER

