#ifndef ENV_HPP
#define ENV_HPP

#include <memory>
#include <unordered_map>
#include <string>


namespace fnl
{
	class Node;

	struct EnvScope
	{
		void declare(std::string const& name, std::shared_ptr<Node> node);
		void update(std::string const& name, std::shared_ptr<Node> node);
		std::shared_ptr<Node> get(std::string const& name);
		
		std::unordered_map<std::string, std::shared_ptr<Node>> table;
		std::unique_ptr<EnvScope> prev;
	};
	
	class Env
	{
  public:
		explicit Env();
		
		void declare(std::string const& name, std::shared_ptr<Node> node);
		void update(std::string const& name, std::shared_ptr<Node> node);
		std::shared_ptr<Node> get(std::string const& name);		

		void open_block();
		void close_block();
		
		virtual ~Env();

		Env(Env const& env) = delete;
		Env& operator=(Env const& env) = delete;

  private:
		std::unique_ptr<EnvScope> m_scope;
	};
}
#endif // ENV

