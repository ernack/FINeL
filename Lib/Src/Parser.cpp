#include <cassert>
#include <iostream>
#include <cmath>
#include "Errors.hpp"
#include "Parser.hpp"
#include "Node.hpp"
#include "Value.hpp"

namespace fnl
{
	/*explicit*/ Parser::Parser()
	{
	}
	
	std::shared_ptr<Node> Parser::operator()(std::vector<Token> const& tokens)
	{
		m_cursor = 0;
		return FINEL(tokens);
	}
	
	/*virtual*/ Parser::~Parser()
	{
	}
	
	std::shared_ptr<Node> Parser::FINEL(std::vector<Token> const& tokens)
	{
		if (tokens.empty()) { return nullptr; }
		
		Value val; val.set_bool(true);
		auto finel = std::make_shared<Node>(NODE_FINEL, val);
		
		while (m_cursor < tokens.size())
			{
				auto expr = EXPR(tokens);

				finel->add_child(expr);
			}

		if (finel->children_count() == 0) { return nullptr; }
		if (finel->children_count() == 1) { return finel->child(0); }
		
		return finel;
	}

	std::shared_ptr<Node> Parser::EXPR(std::vector<Token> const& tokens)
	{
		// VARDECL
		if (tokens.at(m_cursor).type() == TOKEN_OPEN_SQUARE &&
				tokens.at(m_cursor + 1).type() != TOKEN_OPEN_SQUARE)
			{
				m_cursor++;

				Value value; value.set_bool(true);
				auto node = std::make_shared<Node>(NODE_DECLARE_VAR, value);
				
				auto ident = std::make_shared<Node>(NODE_IDENT, tokens.at(m_cursor).value());
				node->add_child(ident);

				m_cursor++;

				auto expr = EXPR(tokens);
				node->add_child(expr);

				m_cursor++;
				
				return node;
			}

		// FUNCALL
		if (tokens.at(m_cursor).type() == TOKEN_OPEN_PAR)
			{
				m_cursor++;
				
				auto node = std::make_shared<Node>(NODE_FUNCALL, tokens.at(m_cursor).value());

				auto ident = std::make_shared<Node>(NODE_IDENT, tokens.at(m_cursor).value());
				node->add_child(ident);

				m_cursor++;
				
				while (m_cursor < tokens.size() &&  tokens.at(m_cursor).type() != TOKEN_CLOSE_PAR)
					{
						auto param = EXPR(tokens);
						node->add_child(param);
					}

				m_cursor++;
				
				return node;
			}
		
		return LITERAL(tokens);
	}

	
	std::shared_ptr<Node> Parser::LITERAL(std::vector<Token> const& tokens)
	{
		// Function
		if (tokens.at(m_cursor).type() == TOKEN_OPEN_SQUARE)
			{
				Value node_value; node_value.set_bool(true);
				auto node = std::make_shared<Node>(NODE_FUNCTION, node_value);

				m_cursor += 2;
				
				auto params = std::make_shared<Node>(NODE_PARAMS, node_value);
				
				while (tokens.at(m_cursor).type() != TOKEN_CLOSE_SQUARE && m_cursor < tokens.size())
					{
						params->add_child(EXPR(tokens));
					}
				
				m_cursor++;
				
				node->add_child(params);

				auto body = std::make_shared<Node>(NODE_BODY, node_value);
				body->add_child(EXPR(tokens));
				node->add_child(body);
				m_cursor++;
				return node;
			}

		switch (tokens.at(m_cursor).type())
			{
			case TOKEN_INT: return std::make_shared<Node>(NODE_INT, tokens.at(m_cursor++).value());
			case TOKEN_BYTE: return std::make_shared<Node>(NODE_BYTE, tokens.at(m_cursor++).value());
			case TOKEN_UINT: return std::make_shared<Node>(NODE_UINT, tokens.at(m_cursor++).value());
			case TOKEN_FLOAT: return std::make_shared<Node>(NODE_FLOAT, tokens.at(m_cursor++).value());
			case TOKEN_BOOL: return std::make_shared<Node>(NODE_BOOL, tokens.at(m_cursor++).value());
			case TOKEN_STRING: return std::make_shared<Node>(NODE_STRING, tokens.at(m_cursor++).value());
			case TOKEN_SYMBOL: return std::make_shared<Node>(NODE_SYMBOL, tokens.at(m_cursor++).value());
			case TOKEN_IDENT: return std::make_shared<Node>(NODE_IDENT, tokens.at(m_cursor++).value());
			default: throw parser_unknown_token_error();
			}
			
		return nullptr;
	}
}
