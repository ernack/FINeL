cmake_minimum_required(VERSION 3.5)

###########
# LIBRARY #
###########

project(FINeL_AST)

file(GLOB_RECURSE
	src
	Src/*.cpp)

add_library(fnl SHARED ${src})

install(TARGETS fnl DESTINATION /usr/lib)
