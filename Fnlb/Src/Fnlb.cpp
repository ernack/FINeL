#include <cassert>
#include <iostream>
#include <iomanip>
#include "Fnlb.hpp"
#include "../../Lib/Src/AST.hpp"
#include "../../Lib/Src/Node.hpp"
#include "../../Fnli/Src/Fnli.hpp"
#include "Errors.hpp"

namespace fnl
{
	/*explicit*/ Fnlb::Fnlb()
		: m_env { std::make_unique<Env>() }
	{
	}
	
	std::string Fnlb::operator()(std::shared_ptr<Node> node)
	{		
		m_first_line = true;
		std::string res = compile(node);
		
		std::stringstream result;

		for (auto const& p : m_functions)
			{
				result << p.second.str();
			}

		if (!res.empty())
			{
				result << ".main " << res;
			}
		
		if (!res.empty()) { result << std::endl; }

		return result.str();
	}
	
	std::string Fnlb::compile(std::shared_ptr<Node> node)
	{
		std::stringstream source;
		std::stringstream functions;
		
		switch (node->type())
			{
			case NODE_FINEL:
				{
					std::string res;
					for (size_t i=0; i<node->children_count(); i++)
						{
							res += compile(node->child(i));

							if (i < node->children_count() - 1 && !res.empty())
								{
									res += "\n";
								}
						}
					
					source << res;
				}
				break;

			case NODE_INT:
				{
					source << "push " + std::to_string(node->value().as_int());
				}
				break;

			case NODE_BYTE:
				{
					source << "push " + std::to_string(node->value().as_byte()) << "b";
				}
				break;

			case NODE_UINT:
				{
					source << "push " + std::to_string(node->value().as_uint()) << "u";
				}
				break;

			case NODE_FLOAT:
				{
					std::stringstream ss; ss << std::setprecision(3) << node->value().as_float();

					if (node->value().as_float() == static_cast<int>(node->value().as_float()))
						{
							ss << ".0";
						}
					
				  source << "push " + ss.str();
				}
				break;

			case NODE_BOOL:
				{
					std::stringstream ss; ss << std::boolalpha << node->value().as_bool();
					source << "push " + ss.str();
				}
				break;

			case NODE_STRING:
				{
				  source << "push \"" + node->value().as_string() + "\"";
				}
				break;


			case NODE_SYMBOL:
				{
					source << "push '" + node->value().as_string();
				}
				break;
				
			case NODE_DECLARE_VAR:
				{
					std::string name = node->child(0)->value().as_string();
					m_env->declare(name, node->child(1));					
				}
				break;
				
			case NODE_IDENT:
				{
					try
						{
							auto n = m_env->get(node->value().as_string());
							source << compile(n);
						}
					catch(...)
						{
							source << "push " << node->value().as_string();
						}

				}
				break;

			case NODE_FUNCTION:
			  compile_function(node);
				break;
				
			case NODE_FUNCALL:
				{					
					std::string function_name = node->value().as_string();					
					size_t arity = node->children_count() - 1;
					
					// Declare
					std::shared_ptr<Node> fct;
					std::vector<std::string> param_names(arity, "?");
					
					try
						// Declared => custom function.
						{
							fct = m_env->get(function_name);
							m_finfo.push_back({static_cast<int>(arity), param_names, function_name});
							
						  compile(fct);														
						}
					catch(std::exception const& e)
						// Not declared => native function.
						{
							for (size_t i = 0; i < arity; i++)
								{
									param_names[arity - 1 - i] = "___x" + std::to_string(m_x_counter++);
								}
						}
					
					// Call
					if (node->child(0)->value().string() == "if")
						{
							int a = m_f_counter++;
							int b = m_f_counter++;
							
							source << compile(node->child(1)) << std::endl;
							source << "jmpf ___f"<< a << std::endl;

							source << compile(node->child(2)) << std::endl;
							source << "jmp ___f" << b << std::endl;
							
							source << ".___f" << a << std::endl;
							
							source << compile(node->child(3)) << std::endl;
							source << ".___f" << b;
							
							break;
						}
					
					for (size_t i=0; i<arity; i++)
						{
							auto child = node->child(arity - i);
							
							if (child->type() == NODE_IDENT)
								{
									try
										{
											std::shared_ptr<Node> fct =
												m_env->get(child->value().as_string());

											if (fct->children_count() > 0)
												{
													m_finfo.push_back({static_cast<int>(fct->child(0)->children_count()),
																						 param_names,
																						 child->value().as_string()});
												}
											else
												{
													m_finfo.push_back({static_cast<int>(fct->children_count()),
																						 param_names,
																						 fct->value().as_string()});
												}
											
										  compile(fct);
											source << "push " << child->value().as_string() << std::endl;
										}
									catch(...)
										{
											source << compile(child) << std::endl;
										}
								}
							else
								{
									source << compile(child) << std::endl;
								}
							
							source << "param " << param_names[i] << std::endl;
						}
					
					source << "call " << function_name;
				}
				break;
				
			default: throw fnlb_unknown_node_error(node_type_string(node->type()));
				
			}

		
		return source.str();		
	}	
	
	Env& Fnlb::env()
	{
		return *m_env;
	}
	
	Env const& Fnlb::env() const
	{
		return *m_env;
	}
	
	/*virtual*/ Fnlb::~Fnlb()
	{
	}

	Value Fnlb::eval(std::shared_ptr<Node> node)
	{
		Fnli fnli;
	  Value val = fnli(".main " + compile(node));
		return val;
	}

	void Fnlb::compile_function(std::shared_ptr<Node> node)
	{
		auto fct = node;
		FunctionInfo info = m_finfo.back();
		m_finfo.pop_back();
					
		int arity = info.arity;					
		std::vector<std::string>& param_names = info.param_names;
		
		if (fct->children_count() > 0)
			{
				auto params = fct->child(0);
							
				for (size_t i=0; i < params->children_count(); i++)
					{
						param_names[i] = params->child(params->children_count() - 1 -i)->value().string();
					}
			}

		{
			auto itr = m_defined.find(info.name);
						
			if (itr != m_defined.end())
				{
					return;
				}

			m_defined[info.name] = true;
		}
		
		m_functions[info.name] << "." << info.name << " ";
		m_env->open_block();
		int push_counter = 0;
		
		for (size_t j=0; j<fct->child(1)->children_count(); j++)
			{
				std::string str = compile(fct->child(1)->child(j));
				
				m_functions[info.name] << str << std::endl;
							
				if (str.substr(0, std::string("push").size()) == "push")
					{
						push_counter++;
					}
			}
							
		m_functions[info.name] << "save" << std::endl;

		for (size_t i=0; i<static_cast<size_t>(arity + push_counter); i++)
			{
				m_functions[info.name] << "pop" << std::endl;
			}
							
		m_functions[info.name] << "ret" << std::endl;

		m_env->close_block();

	}
}
