#ifndef ERRORS_HPP
#define ERRORS_HPP

#include <string>
#include <stdexcept>

namespace fnl
{
	class fnlb_unknown_node_error : public std::runtime_error
	{
	public:
		fnlb_unknown_node_error(std::string name)
			: std::runtime_error("fnlb unknown node error: " + name)
		{
		}
	};

	class fnlb_unknown_node_type_error : public std::runtime_error
	{
	public:
		fnlb_unknown_node_type_error(std::string type)
			: std::runtime_error("fnlb unknown node error: " + type)
		{
		}
	};		
}
#endif // ERRORS

