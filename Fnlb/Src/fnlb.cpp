#include <iostream>
#include "Fnlb.hpp"
#include "../../Lib/Src/AST.hpp"

int main(int argc, char** argv)
{
	std::string line, source;

	while (std::getline(std::cin, line))
		{
			source += line;
			if (!std::cin.eof()) { source += "\n"; }
		}

	fnl::AST ast;
	fnl::Fnlb fnlb;

	std::cout << fnlb(ast(source));
	
	return 0;	
}
