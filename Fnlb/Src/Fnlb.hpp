#ifndef FNLB_HPP
#define FNLB_HPP

#include <map>
#include <unordered_map>
#include <vector>
#include <functional>
#include <sstream>
#include <memory>
#include <string>
#include "../../Lib/Src/Env.hpp"
#include "../../Lib/Src/Value.hpp"

namespace fnl
{
	class Node;

	struct FunctionInfo
	{
		int arity = 0;
		std::vector<std::string>& param_names;
		std::string name;
	};
	
	class Fnlb
	{
  public:
		explicit Fnlb();

		std::string operator()(std::shared_ptr<Node> node);
		std::string compile(std::shared_ptr<Node> node);

		Env const& env() const;
		
		Env& env();
		
		virtual ~Fnlb();
		Fnlb& operator=(Fnlb const& fnlb) = delete;
		Fnlb(Fnlb const& fnlb) = delete;
  private:
		int m_x_counter = 0;
		int m_f_counter = 0;
		bool m_first_line = false;
		std::vector<FunctionInfo> m_finfo;
		std::unique_ptr<Env> m_env;
		std::map<std::string, std::stringstream> m_functions;
		std::stringstream m_source;
		std::unordered_map<std::string, bool> m_defined;
		
		Value eval(std::shared_ptr<Node> node);
		void compile_function(std::shared_ptr<Node> node);
	};
}
#endif // FNLB

