#include <iostream>
#include <sstream>
#include <memory>
#include <catch.hpp>
#include "../Lib/Src/AST.hpp"
#include "../Lib/Src/Env.hpp"
#include "../Lib/Src/Node.hpp"
#include "../Src/Fnlb.hpp"

TEST_CASE("FnlbInt", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("42");

	std::stringstream ss;

	ss << ".main push 42" << std::endl;

	REQUIRE(ss.str() == fnlb(root));
	
}

TEST_CASE("FnlbByte", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("27b");

	std::stringstream ss;

	ss << ".main push 27b" << std::endl;

	REQUIRE(ss.str() == fnlb(root));
	
}

TEST_CASE("FnlbUint", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("78u");

	std::stringstream ss;

	ss << ".main push 78u" << std::endl;

	REQUIRE(ss.str() == fnlb(root));
	
}

TEST_CASE("FnlbFloat", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("3.14");

	std::stringstream ss;

	ss << ".main push 3.14" << std::endl;

	REQUIRE(ss.str() == fnlb(root));
	
}

TEST_CASE("FnlbBooleanTrue", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("true");

	std::stringstream ss;

	ss << ".main push true" << std::endl;

	REQUIRE(ss.str() == fnlb(root));
	
}

TEST_CASE("FnlbBooleanFalse", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("false");

	std::stringstream ss;

	ss << ".main push false" << std::endl;

	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlbString", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("\"hello world\"");

	std::stringstream ss;

	ss << ".main push \"hello world\"" << std::endl;

	REQUIRE(ss.str() == fnlb(root));
	
}

TEST_CASE("FnlbSymbol", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("'paris");

	std::stringstream ss;

	ss << ".main push 'paris" << std::endl;

	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlbSequence", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("'paris 4 3.2");

	std::stringstream ss;

	ss << ".main push 'paris" << std::endl;
	ss << "push 4" << std::endl;
	ss << "push 3.2" << std::endl;

	REQUIRE(ss.str() == fnlb(root));
	
}

TEST_CASE("FnlbDeclareVariable", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("[hello 'world]");

	REQUIRE("" == fnlb(root));	
}

TEST_CASE("FnlIdent", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;

	fnlb.env().declare("hello", ast("32"));
	
	auto root = ast("hello");

	std::stringstream ss;

	ss << ".main push 32" << std::endl;
	
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlFunctionNoParam", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;

	fnlb.env().declare("pi", ast("[[] 3.14]"));
		
	auto root = ast("(pi)");
	
	
	std::stringstream ss;

	ss << ".pi push 3.14" << std::endl;
	ss << "save" << std::endl;
	ss << "pop" << std::endl;
	ss << "ret" << std::endl;
	
	ss << ".main call pi" << std::endl;
	
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlFunctionTwoParams", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	fnlb.env().declare("last", ast("[[x y] y]"));
		
	auto root = ast("(last 8.2 6)");
		
	std::stringstream ss;

	ss << ".last push y" << std::endl;
	ss << "save" << std::endl;
	ss << "pop" << std::endl;
	ss << "pop" << std::endl;
	ss << "pop" << std::endl;
	ss << "ret" << std::endl;
	
	ss << ".main push 6" << std::endl;
	ss << "param y" << std::endl;
	ss << "push 8.2" << std::endl;
	ss << "param x" << std::endl;
	ss << "call last" << std::endl;
	
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlFunctionMoreParams", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	fnlb.env().declare("first", ast("[[x y z] x]"));
		
	auto root = ast("(first 'one 'two 'three)");
		
	std::stringstream ss;

	ss << ".first push x" << std::endl;
	ss << "save" << std::endl;
	ss << "pop" << std::endl; // 'one
	ss << "pop" << std::endl; // 'one
	ss << "pop" << std::endl; // 'two
	ss << "pop" << std::endl; // 'three
	ss << "ret" << std::endl;

	ss << ".main push 'three" << std::endl;
	ss << "param z" << std::endl;
	ss << "push 'two" << std::endl;
	ss << "param y" << std::endl;
	ss << "push 'one" << std::endl;
	ss << "param x" << std::endl;
	ss << "call first" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlFunctionNative", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("(+ 2 4 6)");
		
	std::stringstream ss;

	ss << ".main push 6" << std::endl;
	ss << "param ___x2" << std::endl;
	ss << "push 4" << std::endl;
	ss << "param ___x1" << std::endl;
	ss << "push 2" << std::endl;
	ss << "param ___x0" << std::endl;
	ss << "call +" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlFunctionCallNested", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("(+ 1 (+ 2 3))");
		
	std::stringstream ss;

	ss << ".main push 3" << std::endl;
	ss << "param ___x3" << std::endl;
	ss << "push 2" << std::endl;
	ss << "param ___x2" << std::endl;
	ss << "call +" << std::endl;
	ss << "param ___x1" << std::endl;
	ss << "push 1" << std::endl;
	ss << "param ___x0" << std::endl;
	ss << "call +" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlRoundFloatShouldStayAFloat", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("(+ 1.0 2.0)");
		
	std::stringstream ss;

	ss << ".main push 2.0" << std::endl;
	ss << "param ___x1" << std::endl;
	ss << "push 1.0" << std::endl;
	ss << "param ___x0" << std::endl;
	ss << "call +" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}


TEST_CASE("FnlAssign", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("[x (+ 1 2)] x");
		
	std::stringstream ss;

	ss << ".main push 2" << std::endl;
	ss << "param ___x1" << std::endl;
	ss << "push 1" << std::endl;
	ss << "param ___x0" << std::endl;
	ss << "call +" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlDoubleCall", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("[f [[x] (+ x 1)]] (f (f 4))");
		
	std::stringstream ss;


	ss << ".f push 1" << std::endl;
  ss << "param ___x1" << std::endl;
  ss << "push x" << std::endl;
  ss << "param ___x0" << std::endl;
  ss << "call +" << std::endl;
  ss << "save" << std::endl;
  ss << "pop" << std::endl;
  ss << "pop" << std::endl;
  ss << "ret" << std::endl;
  ss << ".main push 4" << std::endl;
  ss << "param x" << std::endl;
  ss << "call f" << std::endl;
  ss << "param x" << std::endl;
  ss << "call f" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlbIf", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("(if true 'hello 'world)");
		
	std::stringstream ss;


  ss << ".main push true" << std::endl;
	ss << "jmpf ___f0" << std::endl;
  ss << "push 'hello" << std::endl;
	ss << "jmp ___f1" << std::endl;
	ss << ".___f0" << std::endl;
  ss << "push 'world" << std::endl;
	ss << ".___f1" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}

TEST_CASE("FnlbFunctionCallFromFunction", "[FNLB]")
{
	fnl::AST ast;
	fnl::Fnlb fnlb;
	
	auto root = ast("[f [[x] x]] [g [[x] (f x)]] (g 2)");
		
	std::stringstream ss;
	
	ss << ".f push x" << std::endl;
	ss << "save" << std::endl;
	ss << "pop" << std::endl;
	ss << "pop" << std::endl;
	ss << "ret" << std::endl;

	ss << ".g push x" << std::endl;
	ss << "param x" << std::endl;
	ss << "call f" << std::endl;
	ss << "save" << std::endl;
	ss << "pop" << std::endl;
	ss << "pop" << std::endl;
	ss << "ret" << std::endl;

	
  ss << ".main push 2" << std::endl;
	ss << "param x" << std::endl;
  ss << "call g" << std::endl;
		
	REQUIRE(ss.str() == fnlb(root));	
}
