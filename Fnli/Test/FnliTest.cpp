#include <sstream>
#include <catch.hpp>
#include "../Src/Fnli.hpp"
#include "../../Fnlb/Src/Fnlb.hpp"
#include "../../Lib/Src/AST.hpp"

TEST_CASE("FnliConstant", "[Fnli]")
{
	std::stringstream source;
	source << "7" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("7" == fnli(fnlb(ast(source.str()))).string());
}

TEST_CASE("FnliFunction", "[Fnli]")
{
	std::stringstream source;
	source << "[f [[x y z] y]] (f 2 4 6)" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("4" == fnli(fnlb(ast(source.str()))).string());
}

TEST_CASE("FnliUint", "[Fnli]")
{
	std::stringstream source;
	source << "5u" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("5u" == fnli(fnlb(ast(source.str()))).inspect());
}

TEST_CASE("FnliByte", "[Fnli]")
{
	std::stringstream source;
	source << "33b" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("33b" == fnli(fnlb(ast(source.str()))).inspect());
}

TEST_CASE("FnliSimpleDecl", "[Fnli]")
{
	std::stringstream source;
	source << "[f 33] f" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("33" == fnli(fnlb(ast(source.str()))).inspect());
}

TEST_CASE("FnliFunctionReturnArg", "[Fnli]")
{
	std::stringstream source;
	source << "[f [[x] x]] (f 32)" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("32" == fnli(fnlb(ast(source.str()))).string());
}

TEST_CASE("FnliHighOrderFunction", "[Fnli]")
{
	std::stringstream source;
	source << "[pi [[] 3.14]] [go [[f] (f)]] (go pi)" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("3.14" == fnli(fnlb(ast(source.str()))).string());
}

TEST_CASE("FnliReturnFunction", "[Fnli]")
{
	std::stringstream source;
	source << "[second [[] [[x y z] y] ]] [g (second)] (g 1 2 3)" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("2" == fnli(fnlb(ast(source.str()))).string());
}

TEST_CASE("FnliNestedFunctionCall", "[Fnli]")
{
	std::stringstream source;
	source << "[second [[x y z] y]] (second 1 (second 2 4 6) 3)" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("4" == fnli(fnlb(ast(source.str()))).string());
}

TEST_CASE("FnliNestedNativeFunctionCall", "[Fnli]")
{
	std::stringstream source;
	source << "(+ 1 (+ 2 3))" << std::endl;	

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("6" == fnli(fnlb(ast(source.str()))).string());
}

TEST_CASE("FnliSimpleIf", "[Fnli]")
{
	std::stringstream source;
	SECTION("true")
		{
			source << "(if true 4 5)" << std::endl;	

			fnl::AST ast;
			fnl::Fnli fnli;
			fnl::Fnlb fnlb;
	
			REQUIRE("4" == fnli(fnlb(ast(source.str()))).string());
		}

	SECTION("false")
		{
			source << "(if false 4 5)" << std::endl;	

			fnl::AST ast;
			fnl::Fnli fnli;
			fnl::Fnlb fnlb;
	
			REQUIRE("5" == fnli(fnlb(ast(source.str()))).string());
		}

	SECTION("nested")
		{
			source << "(if true (if false 2 3) 4)" << std::endl;	

			fnl::AST ast;
			fnl::Fnli fnli;
			fnl::Fnlb fnlb;
			
			REQUIRE("3" == fnli(fnlb(ast(source.str()))).string());
		}

		SECTION("add-nested")
			{
				source << "(+ (if true 12 15) 4)" << std::endl;	
				
				fnl::AST ast;
				fnl::Fnli fnli;
				fnl::Fnlb fnlb;
				
				REQUIRE("16" == fnli(fnlb(ast(source.str()))).string());
			}
}

TEST_CASE("FnliIdentArg", "[Fnli]")
{
	SECTION("+")
		{
			std::stringstream source;
			source << "[x 7]" << std::endl;
			source << "(+ x 7)" << std::endl;	

			fnl::AST ast;
			fnl::Fnli fnli;
			fnl::Fnlb fnlb;
	
			REQUIRE("14" == fnli(fnlb(ast(source.str()))).string());
		}

		SECTION("last")
		{
			std::stringstream source;
			source << "[last [[x y] y]]" << std::endl;
			source << "[a 7]" << std::endl;
			source << "(last 4 a)" << std::endl;	

			fnl::AST ast;
			fnl::Fnli fnli;
			fnl::Fnlb fnlb;
	
			REQUIRE("7" == fnli(fnlb(ast(source.str()))).string());
		}
}

TEST_CASE("FnlSameParameterUsage", "[Fnli]")
{
	std::stringstream source;
	source << "[f [[x] (+ x 1)]]" << std::endl;
	source << "[g [[x] (* x 2)]]" << std::endl;
	source << "[h [[x] (- x 3)]]" << std::endl;
	source << "(f (g (h 7)))" << std::endl;


	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("9" == fnli(fnlb(ast(source.str()))).string());

}

TEST_CASE("FnlApplyTwice", "[Fnli]")
{
	std::stringstream source;

	source << "[f [[x] (+ x 1)]] (f (f 2))" << std::endl;

	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("4" == fnli(fnlb(ast(source.str()))).string());

}

TEST_CASE("FnlRecursivity", "[Fnli]")
{
	std::stringstream source;

	source << "[fact [[n] (if (eq? n 0) 1 (* n (fact (- n 1))))]] (fact 5)" << std::endl;
	
	fnl::AST ast;
	fnl::Fnli fnli;
	fnl::Fnlb fnlb;
	
	REQUIRE("120" == fnli(fnlb(ast(source.str()))).string());

}
