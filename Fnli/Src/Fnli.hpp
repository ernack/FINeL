#ifndef FNLI_HPP
#define FNLI_HPP

#include <memory>
#include <vector>
#include <map>
#include <string>

#include "../../Lib/Src/Value.hpp"
#include "../../Natives/Src/Native.hpp"

namespace fnl
{
	class Env;
	
	struct Instruction
	{
		std::string name;
		Value value;
	};
	
	class Fnli
	{
  public:
		explicit Fnli();

		Value operator()(std::string const& source);

		virtual ~Fnli();
		
		Fnli& operator=(Fnli const& fnli) = delete;
		Fnli(Fnli const& fnli) = delete;
  private:
		std::unique_ptr<Env> m_env;
		std::vector<Value> m_returned;
		std::vector<int> m_ret_addr;
		std::vector<Value> m_stack;
		std::vector<Instruction> m_instructions;
		std::unordered_map<std::string, int> m_labels;
	  std::vector<std::pair<std::string, Value>> m_params;
		std::unordered_map<std::string, std::unique_ptr<Native>> m_natives;
		
		Value to_value(std::string const& val);
		void run();
	};
}
#endif // FNLI

