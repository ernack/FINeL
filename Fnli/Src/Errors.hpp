#ifndef ERRORS_HPP
#define ERRORS_HPP

#include <stdexcept>

namespace fnl
{
	class fnli_unknown_value_error: public std::runtime_error
	{
	public:
		fnli_unknown_value_error()
			: std::runtime_error("fnli unknown value error")
		{
		}
	};

	class fnli_unknown_native_error: public std::runtime_error
	{
	public:
		fnli_unknown_native_error()
			: std::runtime_error("fnli unknown native error")
		{
		}
	};
}
#endif // ERRORS

