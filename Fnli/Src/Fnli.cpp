#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>
#include "Fnli.hpp"
#include "Errors.hpp"
#include "../../Lib/Src/Env.hpp"
#include "../../Natives/Src/Arith.hpp"
#include "../../Natives/Src/Comp.hpp"

namespace fnl
{
	/*explicit*/ Fnli::Fnli()
	{
		m_env = std::make_unique<Env>();
		m_natives["+"] = std::unique_ptr<Native>(new Add(*m_env));
		m_natives["-"] = std::unique_ptr<Native>(new Sub(*m_env));
		m_natives["*"] = std::unique_ptr<Native>(new Mul(*m_env));
		m_natives["/"] = std::unique_ptr<Native>(new Div(*m_env));

		m_natives["lt?"] = std::unique_ptr<Native>(new Lt(*m_env));
		m_natives["le?"] = std::unique_ptr<Native>(new Le(*m_env));
		m_natives["gt?"] = std::unique_ptr<Native>(new Gt(*m_env));
		m_natives["ge?"] = std::unique_ptr<Native>(new Ge(*m_env));
		m_natives["eq?"] = std::unique_ptr<Native>(new Eq(*m_env));
	}
	
	Value Fnli::operator()(std::string const& source)
	{
		std::stringstream src;
		src << source;

		std::string line;
		int line_counter = 1;
		
		while ( std::getline(src, line) )
			{
				std::stringstream word;
				word << line;
				
				std::string keyword;

				std::string cmd = "";
				std::string value;
				
				while ( std::getline(word, keyword, ' ') )
					{
						if (keyword[0] == '.')
							// Label
							{
								m_labels[keyword.substr(1)] = line_counter;
							}
						else if (cmd.empty())
							{
								cmd = keyword;
							}
						else
							{
								value = keyword;
							}
					}
				
				m_instructions.push_back(Instruction{cmd, to_value(value)});
				line_counter++;
			}

	  run();

		if (m_stack.empty()) { Value val; val.set_bool(false); return val; }
		return m_stack.back();
	}
	
	/*virtual*/ Fnli::~Fnli()
	{
	}

	Value Fnli::to_value(std::string const& val)
	{
		Value value;
		value.set_int(0);
		
		if (val[0] == '\'') { value.set_symbol(val.substr(1)); }
		else if (val[0] == '"') { value.set_string(val.substr(1, val.size() - 1)); }
		else if (val.find(".") != std::string::npos)
			{
				value.set_float(std::stof(val));
			}
		else if ((val[0] >= '0' && val[0] <= '9') &&
						 val.find("b") != std::string::npos)
			{
				value.set_byte(std::stoi(val.substr(0, val.size() - 1)));
			}
		else if ((val[0] >= '0' && val[0] <= '9') &&
						 val.find("u") != std::string::npos)
			{
				value.set_uint(std::stoi(val.substr(0, val.size() - 1)));
			}
		else if (val[0] >= '0' && val[0] <= '9')
			{
				value.set_int(std::stoi(val));
			}

		else if (val == "true" || val == "false")
			{
				value.set_bool(val == "true");
			}
		else
			{
				value.set_string(val);
			}
		
		return value;
	}

	void Fnli::run()
	{

		size_t cursor = m_labels["main"] - 1;
		
		while (cursor < m_instructions.size())
			{
				// std::cout << "----------------" << std::endl;
				// for (auto const& p : m_stack)
				// 	{
				// 		std::cout << "\t" << p.string() << std::endl;
				// 	}
				
				auto const& instr = m_instructions.at(cursor);
				
				if (instr.name == "save")
					{
						m_returned.push_back(m_stack.back());
					}

				if (instr.name == "jmp")
					{
						cursor = m_labels[instr.value.string()] - 1;
						continue;
					}

				if (instr.name == "jmpf")
					{
					  if (m_stack.back().type() == VALUE_BOOL && !m_stack.back().as_bool())
							{
								cursor = m_labels[instr.value.string()];
							}
						else
							{
								cursor++;
							}
						continue;
					}

				if (instr.name == "pop")
					{
						m_stack.pop_back();
					}

				if (instr.name == "param")
					{
						m_params.push_back({instr.value.string(), m_stack.back()});
					}
				
				if (instr.name == "push")
					{
						if (instr.value.type() == VALUE_STRING)
							{
								std::string name = instr.value.as_string();
								
								bool found = false;
								Value val;

								for (size_t i=0; i<m_params.size(); i++)
									{
										auto p = m_params[i];
										
										if (name == p.first)
											{
												if (p.second.type() == VALUE_STRING)
													{
														name = p.second.string();
														i = 0;
														continue;
													}
								
												val = p.second;
												found = true;
											}
									}
								
								if (!found)
									{
										val = instr.value;
									}

								m_stack.push_back(val);
							}
						else
							{
								m_stack.push_back(instr.value);
							}
					}
				
				if (instr.name == "call")
					{
						m_env->open_block();
						std::string fct = instr.value.as_string();
						
						
						for(size_t i=0; i<m_params.size(); i++)
							{
								auto p = m_params[i];
								if (p.first == fct)
								{
									fct = p.second.string();
									i = 0;
								}
							}
						
						auto itr = std::find_if(m_labels.begin(),
																		m_labels.end(),
																		[fct](auto const& p){ return p.first == fct;});
						
						if (itr != m_labels.end())
							// user defined
							{
								m_ret_addr.push_back(cursor);						
								cursor = m_labels[fct] - 1;								
								continue;
							}
						else
							// native
							{
								bool found = false;
								
								for (auto const& native : m_natives)
									{
										size_t stack_size = m_stack.size();
										auto& f = native.second;
										
										if (f->name() == fct)
											{
												found = true;
												int arity = f->arity();
												Value result;

												result = f->call(m_params);
												
												for(int i=0; i<arity; i++)
													{
														m_params.pop_back();														
													}
												
												if (arity == -1)
													{
															auto itr =
																std::find_if(m_params.begin(),
																						 m_params.end(),
																						 [](auto const& p){
																							 return p.first.substr(0, std::string("___").size()) == "___";
																						 })
																;
															m_params.erase(itr, m_params.end());
													}
																								
												while (m_stack.size() > stack_size)
													{
														m_stack.pop_back();
													}

												m_stack.push_back(result);
											}
									}

								if (!found)
									{
										throw fnli_unknown_native_error();
									}
							}

					}

				if (instr.name == "ret")
					{
						if (m_ret_addr.size() > 1)
							{
								m_params.pop_back();
							}
						else
							{
								m_params.clear();
							}

						m_env->close_block();
						m_stack.push_back(m_returned.back());
						m_returned.pop_back();
						cursor = m_ret_addr.back();
						m_ret_addr.pop_back();
					}
				
				cursor++;
			}
	}
}
