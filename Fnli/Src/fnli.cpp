#include <iostream>
#include "Fnli.hpp"

int main(int argc, char** argv)
{
	std::string source, line;

	while ( std::getline(std::cin, line) )
		{
			source += line;
			
			if (!std::cin.eof())
				{
					source += "\n";
				}
		}

	fnl::Fnli fnli;

	fnl::Value value = fnli(source);

	std::cout << value.string() << std::endl;
	
	return 0;	
}
