#include <catch.hpp>
#include "../Src/Arith.hpp"
#include "../Src/Comp.hpp"
#include "../../Lib/Src/Env.hpp"

TEST_CASE("NativeArith", "[NATIVES]")
{
	fnl::Env env;
	fnl::Add add {env};
	std::vector<std::pair<std::string, fnl::Value>> params;

	
	SECTION("int")
		{
			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Value result = add.call(params);
			REQUIRE(fnl::VALUE_INT == result.type());
			REQUIRE(16 == result.as_int());
		}

	SECTION("byte")
		{
			{
				fnl::Value val; val.set_byte(7);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_byte(9);
				params.push_back({"___y", val});
			}

			fnl::Value result = add.call(params);
			REQUIRE(fnl::VALUE_BYTE == result.type());
			REQUIRE(16 == result.as_byte());
		}

	SECTION("uint")
		{
			{
				fnl::Value val; val.set_uint(7);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_uint(9);
				params.push_back({"___y", val});
			}

			fnl::Value result = add.call(params);
			REQUIRE(fnl::VALUE_UINT == result.type());
			REQUIRE(16 == result.as_uint());
		}

	SECTION("float")
		{
			{
				fnl::Value val; val.set_float(7.2);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_float(9.3);
				params.push_back({"___y", val});
			}

			fnl::Value result = add.call(params);
			REQUIRE(fnl::VALUE_FLOAT == result.type());
			REQUIRE(Approx(16.5) == result.as_float());
		}
}

TEST_CASE("ArithAll", "[NATIVES]")
{
	fnl::Env env;
	std::vector<std::pair<std::string, fnl::Value>> params;
	
	SECTION("sub")
		{
			fnl::Sub sub {env};
			
			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___y", val});
			}

			fnl::Value result = sub.call(params);
			REQUIRE(fnl::VALUE_INT == result.type());
			REQUIRE(-2 == result.as_int());
		}

	SECTION("mul")
		{
			fnl::Mul mul {env};
			
			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Value result = mul.call(params);
			REQUIRE(fnl::VALUE_INT == result.type());
			REQUIRE(63 == result.as_int());
		}

	SECTION("div")
		{
			fnl::Div div {env};
			
			{
				fnl::Value val; val.set_int(3);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Value result = div.call(params);
			REQUIRE(fnl::VALUE_INT == result.type());
			REQUIRE(3 == result.as_int());
		}
}

TEST_CASE("NativeComparison", "[NATIVES]")
{
	fnl::Env env;
	std::vector<std::pair<std::string, fnl::Value>> params;

	SECTION("lt?")
		{
			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Lt lt {env};
			fnl::Value result = lt.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(false == result.as_bool());
		}

	SECTION("gt?")
		{
			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Gt gt {env};
			fnl::Value result = gt.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(true == result.as_bool());
		}

	SECTION("eq?")
		{
			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Eq eq {env};
			fnl::Value result = eq.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(false == result.as_bool());
		}

		SECTION("lt?_inv")
		{
			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___y", val});
			}

			fnl::Lt lt {env};
			fnl::Value result = lt.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(true == result.as_bool());
		}

	SECTION("gt?_inv")
		{
			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___y", val});
			}

			fnl::Gt gt {env};
			fnl::Value result = gt.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(false == result.as_bool());
		}

	SECTION("eq?_inv")
		{
			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(7);
				params.push_back({"___y", val});
			}

			fnl::Eq eq {env};
			fnl::Value result = eq.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(false == result.as_bool());
		}

	SECTION("lt?_same")
		{
			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Lt lt {env};
			fnl::Value result = lt.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(false == result.as_bool());
		}

	SECTION("gt?_same")
		{
			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Gt gt {env};
			fnl::Value result = gt.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(false == result.as_bool());
		}

	SECTION("eq?_same")
		{
			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___x", val});
			}

			{
				fnl::Value val; val.set_int(9);
				params.push_back({"___y", val});
			}

			fnl::Eq eq {env};
			fnl::Value result = eq.call(params);
			REQUIRE(fnl::VALUE_BOOL == result.type());
			REQUIRE(true == result.as_bool());
		}
}
