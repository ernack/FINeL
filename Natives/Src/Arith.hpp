#ifndef ARITH_HPP
#define ARITH_HPP
#include <iostream>
#include "Native.hpp"

namespace fnl
{
#define NATIVE_ARITH(NAME,SYM,OP)																				\
	class NAME : public Native																						\
	{																																			\
	public:																																\
		explicit NAME(Env& env)																							\
			: Native(env)																											\
		{																																		\
		}																																		\
																																				\
		std::string name() const override																		\
		{																																		\
			return SYM;																												\
		}																																		\
		Value call(std::vector<std::pair<std::string, Value>> params) override \
		{																																		\
			Value result;																											\
																																				\
			if (!params.empty())																							\
				{																																\
					result = params.back().second;																\
				}																																\
																																				\
			for (size_t i=0; i<params.size()-1; i++)													\
				{																																\
					auto param = params[params.size() - i - 2];										\
																																				\
					if (param.first.substr(0, std::string("___").size()) != "___") \
						{																														\
							continue;																									\
						}																														\
																																				\
					if (param.second.type() == VALUE_STRING)											\
						{																														\
							param.second = m_env.get(param.second.string())->value();	\
						}																														\
																																				\
					switch (param.second.type())																	\
						{																														\
						case VALUE_INT:																							\
							result.set_int(result.as_int() OP param.second.as_int());	\
							break;																										\
							\
						case VALUE_BYTE:																						\
							result.set_byte(result.as_byte() OP param.second.as_byte()); \
							break;																										\
																																				\
						case VALUE_UINT:																						\
							result.set_uint(result.as_uint() OP param.second.as_uint()); \
							break;																										\
																																				\
						case VALUE_FLOAT:																						\
							result.set_float(result.as_float() OP param.second.as_float()); \
							break;																										\
																																				\
						default:																										\
							std::cerr << "native function call unknown value " << param.second.inspect() << std::endl; \
							assert(false);																						\
							break;																										\
						}																														\
				}																																\
																																				\
			return result;																										\
		}																																		\
																																				\
		virtual ~NAME()																											\
		{																																		\
		}																																		\
																																				\
		NAME& operator=(NAME const& add) = delete;													\
		NAME(NAME const& add) = delete;																			\
  private:																															\
	};																																		\


	NATIVE_ARITH(Add,"+",+);
	NATIVE_ARITH(Sub,"-",-);
	NATIVE_ARITH(Mul,"*",*);
	NATIVE_ARITH(Div,"/",/);
	
}
#endif // ARITH

