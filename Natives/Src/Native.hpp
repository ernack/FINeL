#ifndef NATIVE_HPP
#define NATIVE_HPP

#include <map>
#include <cassert>
#include <vector>
#include <memory>

#include "../../Lib/Src/Value.hpp"
#include "../../Lib/Src/Node.hpp"
#include "../../Lib/Src/Env.hpp"

namespace fnl
{
	class Native
	{
  public:
		explicit Native(Env& env)
			: m_env { env }
		{
		}
		virtual std::string name() const = 0;
		virtual Value call(std::vector<std::pair<std::string, Value>> args) = 0;
		virtual int arity() const { return -1; }
		virtual ~Native() {}
		
		Native& operator=(Native const& native) = delete;
		Native(Native const& native) = delete;

	protected:
		Env& m_env;
  private:
	};
}
#endif // NATIVE

