#ifndef COMP_HPP
#define COMP_HPP

#include "Native.hpp"

namespace fnl
{

#define COMP(NAME,SYM,OP)																								\
	class NAME : public Native																						\
	{																																			\
	public:																																\
		explicit NAME(Env& env)																							\
			: Native(env)																											\
		{																																		\
		}																																		\
																																				\
		std::string name() const override																		\
		{																																		\
			return SYM;																												\
		}																																		\
																																				\
		Value call(std::vector<std::pair<std::string, Value>> args) override \
		{																																		\
			Value val;																												\
																																				\
			switch (args[args.size() - 1].second.type())											\
				{																																\
				case VALUE_INT :																								\
					val.set_bool(args[args.size() - 1].second.as_int() OP args[args.size() - 2].second.as_int()); \
					break;																												\
																																				\
				case VALUE_BYTE :																								\
					val.set_bool(args[args.size() - 1].second.as_byte() OP args[args.size() - 2].second.as_byte()); \
					break;																												\
																																				\
				case VALUE_UINT :																								\
					val.set_bool(args[args.size() - 1].second.as_uint() OP args[args.size() - 2].second.as_uint()); \
					break;																												\
																																				\
																																				\
				case VALUE_FLOAT :																							\
					val.set_bool(args[args.size() - 1].second.as_float() OP args[args.size() - 2].second.as_float()); \
					break;																												\
																																				\
				default:																												\
					std::cerr << "Cannot compare "																\
										<< args[args.size() - 1].first											\
										<< " " << SYM << " "																\
										<< args[args.size() - 2].first											\
										<< std::endl;																				\
					assert(false);																								\
					break;																												\
				}																																\
																																				\
			return val;																												\
		}																																		\
																																				\
		int arity() const																										\
		{																																		\
			return 2;																													\
		}																																		\
																																				\
		virtual ~NAME()																											\
		{																																		\
		}																																		\
																																				\
		NAME& operator=(NAME const& comp) = delete;													\
		NAME(NAME const& comp) = delete;																		\
  private:																															\
	};																																		\

	COMP(Lt, "lt?", <);
	COMP(Gt, "gt?", >);
	COMP(Le, "le?", <=);
	COMP(Ge, "ge?", >=);
	COMP(Eq, "eq?", ==);
}
#endif // COMP

